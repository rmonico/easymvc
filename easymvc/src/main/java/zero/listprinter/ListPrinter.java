package zero.listprinter;

import java.util.LinkedList;
import java.util.List;

public class ListPrinter {

    // FIXME: Will bug on Windows...
    private static final String LINE_BREAK = "\n";
    private List<Column> columns;
    private Iterable<?> dataList;
    private List<Integer> columnWidths;
    private String entityName;
    private String pluralEntityName;

    public void setEntityName(String entityName, String pluralEntityName) {
        this.entityName = entityName;
        this.pluralEntityName = pluralEntityName;
    }

    public void setColumnDefinitions(List<Column> columns) {
        this.columns = columns;
    }

    public void setData(Iterable<?> dataList) {
        this.dataList = dataList;
    }

    public void print() throws ListPrinterException {
        List<List<StringBuilder>> formattedData = formatData();

        calculateWidths(formattedData);

        printEverything(formattedData);
    }

    private List<List<StringBuilder>> formatData() throws ListPrinterException {
        List<List<StringBuilder>> formattedDataList = new LinkedList<List<StringBuilder>>();

        List<StringBuilder> headerLine = new LinkedList<StringBuilder>();
        for (Column column : columns)
            headerLine.add(new StringBuilder(column.getTitle()));
        formattedDataList.add(headerLine);

        for (Object line : dataList) {
            List<StringBuilder> formattedLine = new LinkedList<StringBuilder>();

            for (Column column : columns) {
                Object rawData = column.getData(line);

                StringBuilder cellData = new StringBuilder(rawData.toString());

                formattedLine.add(cellData);
            }

            formattedDataList.add(formattedLine);
        }

        return formattedDataList;
    }

    private void calculateWidths(List<List<StringBuilder>> formattedData) {
        List<Integer> widths = new LinkedList<Integer>();

        for (int i = 0; i < formattedData.get(0).size(); i++) {
            widths.add(-1);
        }

        for (List<StringBuilder> line : formattedData) {
            for (int columnIndex = 0; columnIndex < line.size(); columnIndex++) {
                StringBuilder column = line.get(columnIndex);

                int dataLength = calculateDataLength(column);

                if (dataLength > widths.get(columnIndex)) {
                    widths.remove(columnIndex);
                    widths.add(columnIndex, dataLength);
                }
            }
        }

        columnWidths = widths;
    }

    private int calculateDataLength(StringBuilder data) {
        List<Integer> lineBreakIndexes = locateLineEnds(data);

        int maxLength = -1;

        for (int i = 0; i < lineBreakIndexes.size(); i++) {
            int lastBreakIndex = (i == 0) ? 0 : lineBreakIndexes.get(i - 1) + 1;
            int nextBreakIndex = lineBreakIndexes.get(i);

            String line = data.substring(lastBreakIndex, nextBreakIndex);

            if (line.length() > maxLength)
                maxLength = line.length();
        }

        return maxLength;
    }

    private List<Integer> locateLineEnds(StringBuilder data) {
        int lineBreakIndex = 0;

        List<Integer> lineBreakIndexes = new LinkedList<Integer>();

        while ((lineBreakIndex = data.indexOf(LINE_BREAK, lineBreakIndex + 1)) > 0) {
            lineBreakIndexes.add(lineBreakIndex);
        }

        lineBreakIndexes.add(data.length());

        return lineBreakIndexes;
    }

    private void printEverything(List<List<StringBuilder>> formattedData) {
        boolean printingHeader = true;

        for (int i = 0; i < formattedData.size(); i++) {
            List<StringBuilder> dataLine = formattedData.get(i);

            List<StringBuilder> lines = createBodyString(dataLine, i == formattedData.size() - 1);

            if (printingHeader) {
                StringBuilder ruler = makeRuler('+');

                lines.add(0, ruler);
                lines.add(ruler);
            }

            for (StringBuilder line : lines)
                System.out.println(line.toString());

            printingHeader = false;
        }

        System.out.println(makeRuler('+'));

        System.out.println();

        int bodyLineCount = formattedData.size() - 1;

        String entity = bodyLineCount == 1 ? entityName : pluralEntityName;

        System.out.println(String.format("%d %s", bodyLineCount, entity));
    }

    private StringBuilder makeRuler(char columnChar) {
        StringBuilder ruler = new StringBuilder();

        ruler.append(columnChar);

        for (int i = 0; i < columnWidths.size(); i++) {
            int width = columnWidths.get(i);

            appendRepeatedString(ruler, "-", width + 2);

            ruler.append(columnChar);
        }

        return ruler;
    }

    private void appendRepeatedString(StringBuilder builder, String string, int count) {
        for (int i = 0; i < count; i++) {
            builder.append(string);
        }
    }

    private List<StringBuilder> createBodyString(List<StringBuilder> dataLine, boolean isLastLine) {
        List<List<StringBuilder>> cells = getCellsWithLines(dataLine);

        int tallestCellLines = calculateTallestCellSize(cells);

        List<StringBuilder> lines = makeLines(cells, tallestCellLines);

        if ((tallestCellLines > 1) && (!isLastLine)) {
            lines.add(makeRuler('|'));
        }

        return lines;
    }

    private List<StringBuilder> makeLines(List<List<StringBuilder>> cells, int tallestCellLines) {
        List<StringBuilder> lines = new LinkedList<StringBuilder>();

        for (int i = 0; i < tallestCellLines; i++) {
            lines.add(new StringBuilder("|"));
        }

        for (int column = 0; column < cells.size(); column++) {
            int width = columnWidths.get(column);

            List<StringBuilder> cellLines = cells.get(column);

            for (int i = 0; i < tallestCellLines; i++) {
                StringBuilder line = lines.get(i);

                line.append(" ");

                StringBuilder content;

                if (i < cellLines.size()) {
                    content = cellLines.get(i);
                } else {
                    content = new StringBuilder();
                }

                appendRepeatedString(content, " ", width - content.length());

                line.append(content);
                line.append(" |");
            }
        }

        return lines;
    }

    private int calculateTallestCellSize(List<List<StringBuilder>> cells) {
        int tallestCellLines = -1;

        for (List<StringBuilder> cell : cells) {
            if (cell.size() > tallestCellLines) {
                tallestCellLines = cell.size();
            }
        }

        return tallestCellLines;
    }

    private List<List<StringBuilder>> getCellsWithLines(List<StringBuilder> dataLine) {
        List<List<StringBuilder>> cells = new LinkedList<List<StringBuilder>>();

        for (StringBuilder data : dataLine) {
            List<StringBuilder> cellLines = new LinkedList<StringBuilder>();

            List<Integer> lineBreakIndexes = locateLineEnds(data);

            for (int i = 0; i < lineBreakIndexes.size(); i++) {
                int lastBreakIndex = (i == 0) ? 0 : lineBreakIndexes.get(i - 1) + 1;
                int nextBreakIndex = lineBreakIndexes.get(i);

                StringBuilder line = new StringBuilder(data.substring(lastBreakIndex, nextBreakIndex));

                cellLines.add(line);
            }

            cells.add(cellLines);

        }
        return cells;
    }
}
