package zero.listprinter;

import java.util.LinkedList;
import java.util.List;

public class IterableConcatenator implements DataExtractor {

    private DataExtractor subExtractor;

    public IterableConcatenator(DataExtractor subExtractor) {
        this.subExtractor = subExtractor;
    }

    @Override
    public Object extract(Object data) throws ListPrinterException {
        List<Object> list = new LinkedList<Object>();

        Iterable<?> iterable = (Iterable<?>) data;

        for (Object item : iterable) {
            Object listItem = subExtractor.extract(item);

            list.add(listItem);
        }

        return list;
    }

}
