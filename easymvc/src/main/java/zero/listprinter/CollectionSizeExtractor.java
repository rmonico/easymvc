package zero.listprinter;

import java.util.Collection;

public class CollectionSizeExtractor implements DataExtractor {

    @Override
    public Object extract(Object data) throws ListPrinterException {
        if (data == null)
            return null;

        Collection<?> collection = (Collection<?>) data;

        return collection.size();
    }

}
