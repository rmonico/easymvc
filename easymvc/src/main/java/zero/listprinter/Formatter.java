package zero.listprinter;

public interface Formatter {

    String format(Object data);

}
