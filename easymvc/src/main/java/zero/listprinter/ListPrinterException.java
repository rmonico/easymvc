package zero.listprinter;

public class ListPrinterException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1926507383271871716L;

    public ListPrinterException(Exception e) {
        super(e);
    }

}
