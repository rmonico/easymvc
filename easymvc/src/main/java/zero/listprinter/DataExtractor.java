package zero.listprinter;

public interface DataExtractor {

    Object extract(Object data) throws ListPrinterException;

}
