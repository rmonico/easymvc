package zero.listprinter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NoParamMethodExtractor implements DataExtractor {

    private String methodName;
    private DataExtractor subExtractor;

    public NoParamMethodExtractor(String methodName) {
        this.methodName = methodName;
    }

    public NoParamMethodExtractor(String methodName, DataExtractor subExtractor) {
        this.methodName = methodName;
        this.subExtractor = subExtractor;
    }

    @Override
    public Object extract(Object data) throws ListPrinterException {
        Method method;

        try {
            if (data == null)
                method = null;
            else
                method = data.getClass().getDeclaredMethod(methodName);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new ListPrinterException(e);
        }

        Object theData;

        if (method == null)
            theData = null;
        else {
            boolean accessible = method.isAccessible();
            method.setAccessible(true);

            try {
                theData = method.invoke(data);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new ListPrinterException(e);
            }

            method.setAccessible(accessible);
        }

        if (subExtractor != null)
            return subExtractor.extract(theData);
        else
            return theData;
    }

}
