package zero.listprinter;

public class IntegerFormatter implements Formatter {

    private static IntegerFormatter instance;

    public static IntegerFormatter getInstance() {
        if (instance == null)
            instance = new IntegerFormatter();

        return instance;
    }

    @Override
    public String format(Object o) {
        Integer data = (Integer) o;

        return data.toString();
    }

}
