package zero.listprinter;

public class IterableFormatter implements Formatter {

    private Formatter itemFormatter;

    public IterableFormatter(Formatter itemFormatter) {
        this.itemFormatter = itemFormatter;
    }

    @Override
    public String format(Object data) {
        StringBuilder builder = new StringBuilder();

        for (Object item : (Iterable<?>) data) {
            builder.append(itemFormatter.format(item));
            builder.append(", ");
        }

        if (builder.length() > 0)
            builder.delete(builder.length() - 2, builder.length());

        return builder.toString();
    }

}
