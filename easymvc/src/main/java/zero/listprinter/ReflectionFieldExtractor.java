package zero.listprinter;

import java.lang.reflect.Field;

public class ReflectionFieldExtractor implements DataExtractor {

    private String fieldName;
    private DataExtractor subExtractor;

    public ReflectionFieldExtractor(String fieldName) {
        this(fieldName, null);
    }

    public ReflectionFieldExtractor(String fieldName, DataExtractor subExtractor) {
        this.fieldName = fieldName;
        this.subExtractor = subExtractor;
    }

    @Override
    public Object extract(Object data) throws ListPrinterException {
        Field field;
        try {
            if (data == null)
                field = null;
            else
                field = data.getClass().getDeclaredField(fieldName.toString());
        } catch (NoSuchFieldException | SecurityException e) {
            throw new ListPrinterException(e);
        }

        Object theData;
        if (field == null)
            theData = null;
        else {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            try {
                theData = field.get(data);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                throw new ListPrinterException(e);
            }
            field.setAccessible(accessible);
        }

        if (subExtractor != null)
            return subExtractor.extract(theData);
        else
            return theData;
    }

}
