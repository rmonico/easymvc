package zero.easymvc;

public interface DependencyManager {

    public Class<?>[] managedClasses();

    public void beforeUse(Class<?> dependencyClass) throws Exception;

    public Object getInstance(Class<?> dependencyClass) throws Exception;

    public void afterUse(Class<?> dependencyClass) throws Exception;

    void handlerFailed(Throwable cause, Class<?> dependencyClass) throws Exception;
}
