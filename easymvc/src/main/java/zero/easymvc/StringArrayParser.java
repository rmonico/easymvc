package zero.easymvc;

public class StringArrayParser implements BeanParser {

    private static final String ITEM_SEPARATOR = ",";

    @Override
    public String[] parse(Object o) throws BeanParserException {
        // Just support string values for now
        String value = BuiltinParsers.convertToString(o);

        return value.split(ITEM_SEPARATOR);
    }

}
