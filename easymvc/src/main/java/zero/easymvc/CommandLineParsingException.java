package zero.easymvc;

public class CommandLineParsingException extends Exception {

    public CommandLineParsingException(String message) {
        super(message);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 2399309677255077761L;

}
