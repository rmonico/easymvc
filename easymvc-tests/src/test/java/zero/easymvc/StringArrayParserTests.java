package zero.easymvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

public class StringArrayParserTests extends AbstractEasyMVCTest {

    public static class MyCommandArguments {
        @PositionalParameter(parser = StringArrayParser.class)
        private String[] array;
    }

    public static class MyCommand {
        @ArgumentsBean
        @Bean
        private MyCommandArguments args;

        @CommandHandler(path = "command")
        public void run() {

        }
    }

    public static class MyRenderer {
        @SuppressWarnings("unused")
        private MyCommandArguments args;

        @Renderer(path = "command")
        public void run() {

        }
    }

    @Test
    public void should_parse_a_string_array() throws EasyMVCException {
        controller.registerCommandHandler(MyCommand.class);
        controller.registerRenderer(MyRenderer.class);

        List<Object> beans = controller.run("command", "first_item,second_item,third_item");

        EasyMVCAssert.assertBeanList(beans, 1);

        MyCommandArguments args = EasyMVCAssert.assertAndGetBean(beans, 0, MyCommandArguments.class);

        assertNotNull(args.array);

        assertEquals(3, args.array.length);

        assertEquals("first_item", args.array[0]);
        assertEquals("second_item", args.array[1]);
        assertEquals("third_item", args.array[2]);
    }
}
