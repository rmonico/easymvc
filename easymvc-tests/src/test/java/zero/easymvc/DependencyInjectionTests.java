package zero.easymvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DependencyInjectionTests extends AbstractEasyMVCTest {

    public static class TheDependency {
        public boolean prepared;
        public boolean used;
        public boolean failed;
        public boolean disposed;

    }

    public static class DependencyManagerImpl implements DependencyManager {

        private TheDependency dependency;

        public DependencyManagerImpl() {
            dependency = new TheDependency();
        }

        @Override
        public Class<?>[] managedClasses() {
            return new Class<?>[] { TheDependency.class };
        }

        @Override
        public TheDependency getInstance(Class<?> dependencyClass) {
            return dependency;
        }

        @Override
        public void beforeUse(Class<?> dependencyClass) {
            dependency.prepared = true;
        }

        @Override
        public void afterUse(Class<?> dependencyClass) {
            dependency.disposed = true;
        }

        @Override
        public void handlerFailed(Throwable cause, Class<?> dependencyClass) {
            dependency.failed = true;
        }

    }

    public static class Arguments {
        @TokenParameter(token = "--fail")
        private boolean fail;
    }

    public static class Command {

        @ArgumentsBean
        private Arguments args;

        @Dependency
        private TheDependency dependency;

        @CommandHandler(path = { "command" })
        public void execute() {
            if (args.fail)
                throw new RuntimeException("Exception inside command.");

            dependency.used = true;
        }

        @Renderer(path = { "command" })
        public void render() {

        }

    }

    @Test(expected = RuntimeException.class)
    public void should_throw_exception_when_a_commands_dependes_on_a_unmanaged_dependency() {
        controller.registerCommandHandler(Command.class);
    }

    @Test
    public void should_inject_dependency_by_annotation() throws EasyMVCException {
        DependencyManagerImpl dependencyManager = new DependencyManagerImpl();

        controller.addDependencyManager(dependencyManager);

        controller.registerCommandHandler(Command.class);

        controller.registerRenderer(Command.class);

        controller.run("command");

        TheDependency dependency = dependencyManager.getInstance(TheDependency.class);

        assertNotNull(dependency);
        assertTrue(dependency.prepared);
        assertTrue(dependency.used);
        assertTrue(dependency.disposed);
    }

    @Test
    public void should_call_dependencyManager_fail_method() throws EasyMVCException {
        DependencyManagerImpl dependencyManager = new DependencyManagerImpl();

        controller.addDependencyManager(dependencyManager);

        controller.registerCommandHandler(Command.class);

        controller.registerRenderer(Command.class);

        try {
            controller.run("command", "--fail");
        } catch (EasyMVCException e) {
            Throwable cause = e.getCause();
            assertEquals("Exception inside command.", cause.getMessage());
        }

        TheDependency dependency = dependencyManager.getInstance(TheDependency.class);

        assertNotNull(dependency);
        assertTrue(dependency.prepared);
        assertFalse(dependency.used);
        assertTrue(dependency.failed);
        assertFalse(dependency.disposed);
    }
}
