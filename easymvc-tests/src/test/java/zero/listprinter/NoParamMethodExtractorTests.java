package zero.listprinter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

public class NoParamMethodExtractorTests {

    private DataExtractor extractor;

    @Before
    public void setup() {
        extractor = new NoParamMethodExtractor("getData");
    }

    @Test
    public void should_extract_data_from_method() throws ListPrinterException {
        Object dataClass = new Object() {

            @SuppressWarnings("unused")
            public String getData() {
                return "extracted data";
            }
        };

        assertEquals("Extracted data", "extracted data", extractor.extract(dataClass));
    }

    @Test
    public void should_return_null_when_extracting_from_null() throws ListPrinterException {
        assertNull("Extracted data", extractor.extract(null));
    }

    @Test
    public void should_extract_from_non_accessible_method() throws ListPrinterException {
        Object dataClass = new Object() {

            @SuppressWarnings("unused")
            private String getData() {
                return "private extracted data";
            }
        };

        assertEquals("Extracted data", "private extracted data", extractor.extract(dataClass));
    }

    @Test
    public void should_extract_from_subextractor() throws ListPrinterException {
        final Object subdataClass = new Object() {

            @SuppressWarnings("unused")
            private String getSubData() {
                return "final extracted data";
            }
        };

        Object dataClass = new Object() {

            @SuppressWarnings("unused")
            private Object getData() {
                return subdataClass;
            }
        };

        extractor = new NoParamMethodExtractor("getData", new NoParamMethodExtractor("getSubData"));

        assertEquals("Extracted data", "final extracted data", extractor.extract(dataClass));
    }
}
