package zero.listprinter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class IterableConcatenatorTests {

    public static class SubClass {
        public String data;

        public SubClass(String data) {
            this.data = data;
        }
    }

    @Test
    public void should_concatenate_a_iterable_itens() throws ListPrinterException {
        DataExtractor extractor = new IterableConcatenator(new ReflectionFieldExtractor("data"));

        List<SubClass> data = new LinkedList<SubClass>();

        data.add(new SubClass("First item"));
        data.add(new SubClass("2nd item"));
        data.add(new SubClass("Terceiro item"));

        @SuppressWarnings("unchecked")
        List<Object> extracted = (List<Object>) extractor.extract(data);

        assertNotNull("Valid list", extracted);
        assertEquals("Size", 3, extracted.size());
        assertEquals("First item", "First item", extracted.get(0));
        assertEquals("Second item", "2nd item", extracted.get(1));
        assertEquals("Third item", "Terceiro item", extracted.get(2));
    }
}
