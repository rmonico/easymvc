package zero.listprinter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CollectionSizeExtractorTests {

    private DataExtractor extractor;

    @Before
    public void setup() {
        extractor = new CollectionSizeExtractor();
    }

    @Test
    public void should_extract_size_from_collection() throws ListPrinterException {
        List<Object> data = new LinkedList<Object>();

        data.add("");
        data.add(3);
        data.add('f');

        assertEquals("Extracted data", 3, extractor.extract(data));
    }

    @Test
    public void should_extract_from_null() throws ListPrinterException {
        assertNull("Extracted data", extractor.extract(null));
    }
}
