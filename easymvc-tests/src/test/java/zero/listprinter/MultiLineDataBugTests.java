package zero.listprinter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import zero.utils.sysoutwrapper.SysoutWrapper;

public class MultiLineDataBugTests {

    private ListPrinter printer;
    private SysoutWrapper wrapper;

    public static class Line {
        private static int lastId;
        public int id;
        public String data;
        public String data2;

        public Line(String data) {
            this.data = data;
            id = lastId;

            lastId++;
        }

        public Line(String data, String data2) {
            this(data);
            this.data2 = data2;
        }

        public static void resetId() {
            lastId = 0;
        }
    }

    @Before
    public void before() {
        printer = new ListPrinter();

        printer.setEntityName("Line", "Lines");

        wrapper = new SysoutWrapper();
        System.setOut(wrapper);
    }

    private List<Column> createColumnDefinitions() {
        List<Column> definitions = new LinkedList<Column>();

        definitions.add(new FormattedColumn("ID", new ReflectionFieldExtractor("id"), IDFormatter.getInstance()));
        definitions.add(new FormattedColumn("Data", new ReflectionFieldExtractor("data"), StringFormatter.getInstance()));

        return definitions;
    }

    @Test
    public void should_print_data_with_breaks_without_break_entire_line() throws ListPrinterException {
        printer.setColumnDefinitions(createColumnDefinitions());

        List<Line> data = new LinkedList<Line>();

        Line.resetId();

        data.add(new Line("Single data"));
        data.add(new Line("Multi\nline"));
        data.add(new Line("Another single data"));

        printer.setData(data);

        printer.print();

        assertThat("Header", wrapper.capturedLines.get(1), is("| ID | Data                |"));
        assertThat("Data #0", wrapper.capturedLines.get(3), is("| #0 | Single data         |"));
        assertThat("Data #1", wrapper.capturedLines.get(4), is("| #1 | Multi               |"));
        assertThat("Data #2", wrapper.capturedLines.get(5), is("|    | line                |"));
        assertThat("Data #3", wrapper.capturedLines.get(6), is("|----|---------------------|"));
        assertThat("Data #4", wrapper.capturedLines.get(7), is("| #2 | Another single data |"));
    }

    @Test
    public void should_print_data_with_breaks_on_more_than_one_column_without_break_entire_line() throws ListPrinterException {
        List<Column> columns = createColumnDefinitions();
        columns.add(new FormattedColumn("Data 2", new ReflectionFieldExtractor("data2"), StringFormatter.getInstance()));

        printer.setColumnDefinitions(columns);

        List<Line> data = new LinkedList<Line>();

        Line.resetId();

        data.add(new Line("Single data", "Single data 2"));
        data.add(new Line("Multi\nline data 1", "Multi\nline\ndata\n2, with the greather line"));
        data.add(new Line("Another single data", "Another single data 2"));

        printer.setData(data);

        printer.print();

        assertThat("Header", wrapper.capturedLines.get(1), is("| ID | Data                | Data 2                    |"));
        assertThat("Data #0", wrapper.capturedLines.get(3), is("| #0 | Single data         | Single data 2             |"));
        assertThat("Data #1", wrapper.capturedLines.get(4), is("| #1 | Multi               | Multi                     |"));
        assertThat("Data #2", wrapper.capturedLines.get(5), is("|    | line data 1         | line                      |"));
        assertThat("Data #3", wrapper.capturedLines.get(6), is("|    |                     | data                      |"));
        assertThat("Data #4", wrapper.capturedLines.get(7), is("|    |                     | 2, with the greather line |"));
        assertThat("Data #5", wrapper.capturedLines.get(8), is("|----|---------------------|---------------------------|"));
        assertThat("Data #6", wrapper.capturedLines.get(9), is("| #2 | Another single data | Another single data 2     |"));
    }

    @Test
    public void should_dont_print_multi_line_separator_for_last_line() throws ListPrinterException {
        printer.setColumnDefinitions(createColumnDefinitions());

        List<Line> data = new LinkedList<Line>();

        Line.resetId();

        data.add(new Line("Multi\nline"));

        printer.setData(data);

        printer.print();

        assertThat("Header", wrapper.capturedLines.get(1), is("| ID | Data  |"));
        assertThat("Data #0", wrapper.capturedLines.get(2), is("+----+-------+"));
        assertThat("Data #1", wrapper.capturedLines.get(3), is("| #0 | Multi |"));
        assertThat("Data #2", wrapper.capturedLines.get(4), is("|    | line  |"));
        assertThat("Data #3", wrapper.capturedLines.get(5), is("+----+-------+"));
    }
}
