package zero.easymvc.ormlite.factory;

import java.io.IOException;
import java.sql.SQLException;

import zero.easymvc.EasyMVC;
import zero.easymvc.ormlite.ConnectionManager;
import zero.easymvc.ormlite.DaoManager;
import zero.easymvc.ormlite.DatabaseUpdater;
import ch.qos.logback.classic.Logger;

public abstract class AbstractApplicationFactory {

    protected Logger logger;
    protected ConnectionManager connectionManager;
    protected EasyMVC controller;
    protected DaoManager daoManager;

    public void makeProperties() throws IOException {
    }

    public Logger makeLogger() {
        logger = createLogger();

        return logger;
    }

    protected abstract Logger createLogger();

    public ConnectionManager makeConnectionManager() throws SQLException {
        connectionManager = createConnectionManager();

        return connectionManager;
    }

    protected abstract ConnectionManager createConnectionManager() throws SQLException;

    public DaoManager makeDaoManager() {
        daoManager = createDaoManager();

        return daoManager;
    }

    protected abstract DaoManager createDaoManager();

    public EasyMVC makeController() throws SQLException {
        controller = createController();

        return controller;
    }

    protected abstract EasyMVC createController() throws SQLException;

    public abstract DatabaseUpdater getDatabaseUpdater();

}
