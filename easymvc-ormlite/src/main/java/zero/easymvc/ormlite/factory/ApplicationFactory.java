package zero.easymvc.ormlite.factory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import zero.easymvc.DependencyManager;
import zero.easymvc.EasyMVC;
import zero.easymvc.ormlite.ConnectionManager;
import zero.easymvc.ormlite.DaoManager;
import zero.easymvc.ormlite.DatabaseUpdaterManager;
import zero.easymvc.ormlite.dao.MetaInfDao;
import zero.easymvc.ormlite.model.MetaInf;
import zero.environment.AbstractEnvironment;
import zero.environment.ApplicationPropertyKeys;
import zero.environment.Environment;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

public abstract class ApplicationFactory extends AbstractApplicationFactory {

    private String applicationBasename;

    public ApplicationFactory(String applicationBasename) {
        this.applicationBasename = applicationBasename;
    }

    public void makeProperties() throws IOException {
        Environment.get().setProperty(ApplicationPropertyKeys.LOGGER_VERBOSITY_PROPERTY_KEY, ApplicationPropertyKeys.DEFAULT_LOGGER_VERBOSITY);

        String homeFolderName = getApplicationHomeFolder();

        Environment.get().setProperty(ApplicationPropertyKeys.APP_HOME_PROPERTY_KEY, homeFolderName);

        File homeFolder = new File(homeFolderName);

        if (!homeFolder.exists())
            homeFolder.mkdirs();

        loadPropertiesFromFile();
    }

    private String getApplicationHomeFolder() {
        String applicationEnvironmentVariable = String.format("%s_HOME", applicationBasename.toUpperCase());

        String applicationHomeFolder = Environment.get().getProperty(applicationEnvironmentVariable);

        if (applicationHomeFolder == null) {
            String homeFolder = Environment.get().getProperty("HOME");

            applicationHomeFolder = String.format("%s/.config/%s", homeFolder, applicationBasename);
        }

        return applicationHomeFolder;
    }

    private void loadPropertiesFromFile() throws IOException {
        File file = getPropertiesFile();

        if (!file.exists())
            return;

        Properties properties = loadPropertiesFromFile(file);

        AbstractEnvironment environment = Environment.get();

        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);

            environment.setProperty(key, value);
        }
    }

    protected File getPropertiesFile() {
        String applicationHomeFolder = Environment.get().getProperty(ApplicationPropertyKeys.APP_HOME_PROPERTY_KEY);

        return new File(String.format("%s/config.properties", applicationHomeFolder));
    }

    private Properties loadPropertiesFromFile(File file) throws FileNotFoundException, IOException {
        InputStream inStream = new FileInputStream(file);

        Properties properties = new Properties();

        properties.load(inStream);

        inStream.close();
        return properties;
    }

    @Override
    protected Logger createLogger() {
        Logger logger = (Logger) org.slf4j.LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

        String verbosity = Environment.get().getProperty(ApplicationPropertyKeys.LOGGER_VERBOSITY_PROPERTY_KEY);

        Level loggerLevel;

        switch (verbosity) {
        case "off":
            loggerLevel = Level.OFF;
            break;

        case "error":
            loggerLevel = Level.ERROR;
            break;

        case "warning":
            loggerLevel = Level.WARN;
            break;

        case "info":
            loggerLevel = Level.INFO;
            break;

        case "trace":
            loggerLevel = Level.TRACE;
            break;

        case "debug":
            loggerLevel = Level.DEBUG;
            break;

        case "all":
            loggerLevel = Level.ALL;
            break;

        default:
            loggerLevel = Level.ERROR;
        }

        logger.setLevel(loggerLevel);

        return logger;
    }

    @Override
    protected ConnectionManager createConnectionManager() throws SQLException {
        return new ConnectionManager();
    }

    @Override
    protected DaoManager createDaoManager() {
        Map<Class<?>, Class<?>> daoInfo = new HashMap<>();

        populateDaoInfo(daoInfo);

        daoManager = createDaoManager(daoInfo);

        return daoManager;
    }

    @Override
    protected EasyMVC createController() throws SQLException {
        EasyMVC controller = new EasyMVC();

        registerDependencyManagers(controller);

        registerCommands(controller);

        registerRenderers(controller);

        return controller;
    }

    protected void populateDaoInfo(Map<Class<?>, Class<?>> daoInfo) {
        daoInfo.put(MetaInfDao.class, MetaInf.class);
    }

    protected DaoManager createDaoManager(Map<Class<?>, Class<?>> daoInfo) {
        daoManager = new DaoManager(daoInfo);

        daoManager.setConnection(connectionManager.getConnection());

        return daoManager;
    }

    private void registerDependencyManagers(EasyMVC controller) throws SQLException {
        List<DependencyManager> dependencyManagers = new LinkedList<>();

        createDependencyManagerList(dependencyManagers);

        if (dependencyManagers != null)
            for (DependencyManager manager : dependencyManagers)
                controller.addDependencyManager(manager);
    }

    protected void createDependencyManagerList(List<DependencyManager> managers) {
        managers.add(daoManager);
        managers.add(connectionManager);
        managers.add(new DatabaseUpdaterManager(getDatabaseUpdater()));
    }

    protected void registerCommands(EasyMVC controller) {
        List<Class<?>> commands = new LinkedList<>();

        createCommandList(commands);

        if (commands != null)
            for (Class<?> command : commands)
                controller.registerCommandHandler(command);
    }

    protected void createCommandList(List<Class<?>> commands) {
    }

    protected void registerRenderers(EasyMVC controller) {
        List<Class<?>> renderers = new LinkedList<>();

        createRendererList(renderers);

        if (renderers != null)
            for (Class<?> renderer : renderers)
                controller.registerRenderer(renderer);
    }

    protected void createRendererList(List<Class<?>> renderers) {
    }

}
