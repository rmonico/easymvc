package zero.easymvc.ormlite.dao;

import java.sql.SQLException;
import java.util.List;

import zero.easymvc.ormlite.model.MetaInf;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

public class MetaInfDao extends AbstractDao<MetaInf> {

    private static final String DATABASE_VERSION_KEY = "database.version";
    private static final String APPLICATION_MAJOR_VERSION_KEY = "app.version.major";
    private static final String APPLICATION_MINOR_VERSION_KEY = "app.version.minor";

    public MetaInfDao(ConnectionSource connection, Class<MetaInf> dataClass) throws SQLException {
        super(connection, dataClass);
    }

    public static MetaInfDao getInstance(ConnectionSource connection) throws SQLException {
        return (MetaInfDao) AbstractDao.getInstance(connection, MetaInf.class);
    }

    public MetaInf getValue(String key) throws SQLException {
        QueryBuilder<MetaInf, Integer> builder = queryBuilder();

        Where<MetaInf, Integer> where = builder.where();

        where.eq(MetaInf.KEY_FIELD_NAME, key);

        PreparedQuery<MetaInf> query = builder.prepare();

        List<MetaInf> keys = query(query);

        if (keys.isEmpty())
            return null;

        return keys.get(0);
    }

    public MetaInf createOrUpdateKey(String key, String newValue) throws SQLException {
        MetaInf value = getValue(key);

        MetaInf originalValue;

        if (value == null) {
            value = new MetaInf();
            value.setKey(key);

            originalValue = null;
        } else {
            originalValue = new MetaInf(value);
        }

        value.setValue(newValue);

        createOrUpdate(value);

        return originalValue;
    }

    public int getDatabaseVersion() throws SQLException {
        MetaInf dbVersion = getValue(DATABASE_VERSION_KEY);

        if (dbVersion == null)
            return -1;

        return Integer.parseInt(dbVersion.getValue());
    }

    public void updateDatabaseVersion(int databaseVersion) throws SQLException {
        String databaseVersionStr = new Integer(databaseVersion).toString();

        createOrUpdateKey(DATABASE_VERSION_KEY, databaseVersionStr);
    }

    public void updateApplicationVersion(int major, int minor) throws SQLException {
        String majorStr = new Integer(major).toString();
        String minorStr = new Integer(minor).toString();

        createOrUpdateKey(APPLICATION_MAJOR_VERSION_KEY, majorStr);
        createOrUpdateKey(APPLICATION_MINOR_VERSION_KEY, minorStr);
    }

}
