package zero.easymvc.ormlite;

import java.sql.SQLException;

import zero.easymvc.DependencyManager;
import zero.environment.ApplicationPropertyKeys;
import zero.environment.Environment;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

public class ConnectionManager implements DependencyManager {

    public static final String DEFAULT_SQLITE_JDBC_DRIVER = "org.sqlite.JDBC";

    private ConnectionSource connectionSource;

    public ConnectionManager() throws SQLException {
        connectionSource = new JdbcConnectionSource(getConnectionString());

        Environment.get().setProperty(ApplicationPropertyKeys.JDBC_DRIVER_CLASS_KEY, DEFAULT_SQLITE_JDBC_DRIVER);
    }

    public String getConnectionString() {
        String jdbcUrl = Environment.get().getProperty(ApplicationPropertyKeys.JDBC_URL_KEY);

        if (jdbcUrl == null) {
            jdbcUrl = String.format("jdbc:sqlite:%s/%s", Environment.get().getProperty(ApplicationPropertyKeys.APP_HOME_PROPERTY_KEY), "database.sqlite");
            Environment.get().setProperty(ApplicationPropertyKeys.JDBC_URL_KEY, jdbcUrl);
        }

        return jdbcUrl;
    }

    public ConnectionSource getConnection() {
        return connectionSource;
    }

    @Override
    public Class<?>[] managedClasses() {
        return new Class[] { ConnectionSource.class };
    }

    @Override
    public void beforeUse(Class<?> dependencyClass) {
    }

    @Override
    public Object getInstance(Class<?> dependencyClass) throws Exception {
        return connectionSource;
    }

    @Override
    public void afterUse(Class<?> dependencyClass) throws Exception {

    }

    @Override
    public void handlerFailed(Throwable cause, Class<?> dependencyClass) {
    }

}
