package zero.easymvc.ormlite;

import zero.easymvc.DependencyManager;

public class DatabaseUpdaterManager implements DependencyManager {

    private DatabaseUpdater updater;

    public DatabaseUpdaterManager(DatabaseUpdater updater) {
        this.updater = updater;
    }

    @Override
    public Class<?>[] managedClasses() {
        return new Class<?>[] { DatabaseUpdater.class };
    }

    @Override
    public void beforeUse(Class<?> dependencyClass) throws Exception {
    }

    @Override
    public Object getInstance(Class<?> dependencyClass) throws Exception {
        return updater;
    }

    @Override
    public void afterUse(Class<?> dependencyClass) throws Exception {
    }

    @Override
    public void handlerFailed(Throwable cause, Class<?> dependencyClass) throws Exception {
    }

}
