package zero.easymvc.ormlite;

import java.sql.SQLException;

import zero.easymvc.ormlite.model.MetaInf;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class MetaInfUpdater extends AbstractDatabaseUpdater {

    public MetaInfUpdater(ConnectionSource connection) {
        super(connection);

        setUpdaterVersion(0);
        setApplicationMajorVersion(0);
        setApplicationMinorVersion(0);
    }

    @Override
    public void updateStructure() throws SQLException {
        TableUtils.createTableIfNotExists(connection, MetaInf.class);
    }

}
