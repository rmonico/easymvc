package zero.easymvc.ormlite;

import java.sql.SQLException;

public interface DatabaseUpdater {

    void update(int databaseVersion) throws SQLException;

    boolean shouldUpdate();

    int getUpdaterVersion();

    int getApplicationMajorVersion();

    int getApplicationMinorVersion();

}
