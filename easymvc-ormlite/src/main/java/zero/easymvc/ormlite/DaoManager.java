package zero.easymvc.ormlite;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Map;

import zero.easymvc.DependencyManager;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;

public class DaoManager implements DependencyManager {

    private ConnectionSource source;
    private DatabaseConnection connection;
    private Savepoint savePoint;
    private Map<Class<?>, Class<?>> daoInfo;
    private boolean inUse;

    public DaoManager(Map<Class<?>, Class<?>> daoInfo) {
        this.daoInfo = daoInfo;
    }

    public void setConnection(ConnectionSource source) {
        this.source = source;
    }

    @Override
    public Class<?>[] managedClasses() {
        return daoInfo.keySet().toArray(new Class<?>[] {});
    }

    @Override
    public void beforeUse(Class<?> dependencyClass) throws SQLException {
        connection = source.getReadWriteConnection();

        connection.setAutoCommit(false);

        if (!inUse) {
            savePoint = connection.setSavePoint("DaoManager beforeUse save point");

            inUse = true;
        }
    }

    @Override
    public Object getInstance(Class<?> daoClass) throws Exception {
        Class<?> dataClass = daoInfo.get(daoClass);

        return com.j256.ormlite.dao.DaoManager.createDao(source, dataClass);
    }

    @Override
    public void afterUse(Class<?> daoClass) throws Exception {
        if (inUse) {
            connection.commit(savePoint);

            inUse = false;
        }
    }

    @Override
    public void handlerFailed(Throwable causa, Class<?> dependencyClass) throws SQLException {
        connection.rollback(savePoint);
    }

}
