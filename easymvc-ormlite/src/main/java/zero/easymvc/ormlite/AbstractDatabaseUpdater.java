package zero.easymvc.ormlite;

import java.sql.SQLException;

import zero.easymvc.ormlite.dao.MetaInfDao;

import com.j256.ormlite.support.ConnectionSource;

public abstract class AbstractDatabaseUpdater implements DatabaseUpdater {

    protected ConnectionSource connection;
    protected int oldVersion;
    protected int databaseVersion;
    private int applicationMajorVersion;
    private int applicationMinorVersion;
    private int updaterVersion;

    public AbstractDatabaseUpdater(ConnectionSource connection) {
        this.connection = connection;
    }

    @Override
    public void update(int databaseVersion) throws SQLException {
        this.databaseVersion = databaseVersion;

        if (!shouldUpdate())
            return;

        updateStructure();

        updateDatabaseVersionMetadata();
    }

    public abstract void updateStructure() throws SQLException;

    public void updateDatabaseVersionMetadata() throws SQLException {
        MetaInfDao dao = MetaInfDao.getInstance(connection);

        dao.updateDatabaseVersion(getUpdaterVersion());

        int applicationCurrentMajorVersion = getApplicationMajorVersion();
        int applicationCurrentMinorVersion = getApplicationMinorVersion();

        dao.updateApplicationVersion(applicationCurrentMajorVersion, applicationCurrentMinorVersion);
    }

    @Override
    public boolean shouldUpdate() {
        return getUpdaterVersion() > databaseVersion;
    }

    @Override
    public int getUpdaterVersion() {
        return updaterVersion;
    }

    public void setUpdaterVersion(int updaterVersion) {
        this.updaterVersion = updaterVersion;
    }

    @Override
    public int getApplicationMajorVersion() {
        return applicationMajorVersion;
    }

    public void setApplicationMajorVersion(int applicationMajorVersion) {
        this.applicationMajorVersion = applicationMajorVersion;
    }

    @Override
    public int getApplicationMinorVersion() {
        return applicationMinorVersion;
    }

    public void setApplicationMinorVersion(int applicationMinorVersion) {
        this.applicationMinorVersion = applicationMinorVersion;
    }

}
