package zero.environment;

public class SystemReadOnlyEnvironment implements AbstractEnvironment {

    @Override
    public String getProperty(String property) {
        return System.getenv(property);
    }

    @Override
    public void setProperty(String property, String value) {
    }

}
