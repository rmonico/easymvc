package zero.environment;

public class ApplicationPropertyKeys {

    public static final String JDBC_URL_KEY = "jdbc_url";
    public static final String JDBC_DRIVER_CLASS_KEY = "jdbc_driver_class";

    public static final String EXECUTABLE_MAJOR_VERSION_PROPERTY_KEY = "major_version";
    public static final String EXECUTABLE_MINOR_VERSION_PROPERTY_KEY = "minor_version";
    public static final String EXECUTABLE_PROJECT_PHASE_PROPERTY_KEY = "project_phase";

    public static final String LOGGER_VERBOSITY_PROPERTY_KEY = "logger_verbosity";
    public static final String DEFAULT_LOGGER_VERBOSITY = "error";

    public static final String APP_HOME_PROPERTY_KEY = "app_home";
}
