package zero.environment;

import java.util.HashMap;
import java.util.Map;

public class VolatileEnvironment implements AbstractEnvironment {

    private Map<String, String> properties;
    private AbstractEnvironment wrappedEnvironment;

    public VolatileEnvironment(AbstractEnvironment wrappedEnvironment) {
        this.properties = new HashMap<String, String>();
        this.wrappedEnvironment = wrappedEnvironment;
    }

    @Override
    public String getProperty(String property) {
        String value = properties.get(property);

        if (value != null)
            return value;
        else
            return wrappedEnvironment.getProperty(property);
    }

    @Override
    public void setProperty(String property, String value) {
        properties.put(property, value);
    }

}
