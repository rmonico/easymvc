package zero.environment;

public class Environment implements AbstractEnvironment {

    private static AbstractEnvironment instance;

    public static void set(AbstractEnvironment newEnvironment) {
        instance = newEnvironment;
    }

    public static AbstractEnvironment get() {
        if (instance == null)
            set(createDefaultEnvironment());

        return instance;
    }

    private static AbstractEnvironment createDefaultEnvironment() {
        AbstractEnvironment systemEnvironment = new SystemReadOnlyEnvironment();

        return new VolatileEnvironment(systemEnvironment);
    }

    @Override
    public String getProperty(String property) {
        return instance.getProperty(property);
    }

    @Override
    public void setProperty(String property, String value) {
        instance.setProperty(property, value);
    }

}
