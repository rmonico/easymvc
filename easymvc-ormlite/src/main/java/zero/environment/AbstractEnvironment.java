package zero.environment;

public interface AbstractEnvironment {

    String getProperty(String property);

    void setProperty(String property, String value);

}
