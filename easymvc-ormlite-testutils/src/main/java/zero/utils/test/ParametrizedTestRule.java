package zero.utils.test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.junit.rules.ExternalResource;
import org.junit.rules.TestName;

public abstract class ParametrizedTestRule<T extends Annotation> extends ExternalResource {

    private TestName testName;
    private Object testClass;
    private Class<T> annotationClass;

    public ParametrizedTestRule(TestName testName, Object testClass, Class<T> annotationClass) {
        this.testName = testName;
        this.testClass = testClass;
        this.annotationClass = annotationClass;
    }

    @Override
    protected void before() throws Throwable {
        super.before();

        String methodName = testName.getMethodName();

        Class<? extends Object> theTestClass = testClass.getClass();

        Method method = theTestClass.getMethod(methodName);

        T annotation = method.getAnnotation(annotationClass);

        if (annotation == null)
            annotation = theTestClass.getAnnotation(annotationClass);

        doBefore(annotation);
    }

    protected abstract void doBefore(T testParameter);

}
