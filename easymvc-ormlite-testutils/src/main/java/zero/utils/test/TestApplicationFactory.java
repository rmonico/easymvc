package zero.utils.test;

import zero.easymvc.ormlite.DatabaseUpdater;

public interface TestApplicationFactory {

    public DatabaseUpdater getBeforeTestsDatabaseUpdater();

}
