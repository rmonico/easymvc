package zero.utils.test;

import java.util.List;

import zero.easymvc.ormlite.DatabaseUpdater;

public class AbstractTestApplicationFactory implements TestApplicationFactory {

    public static final int DATABASE_LAST_VERSION = -2;
    private static final int NO_UPDATER = -1;
    private int databaseVersion;
    private List<DatabaseUpdater> updaters;

    public AbstractTestApplicationFactory(int databaseVersion) {
        this.databaseVersion = databaseVersion;
    }

    public void addUpdaters(List<DatabaseUpdater> updaters) {
        this.updaters = updaters;
    }

    public DatabaseUpdater getDatabaseUpdater() {
        return getDatabaseUpdater(databaseVersion);
    }

    @Override
    public DatabaseUpdater getBeforeTestsDatabaseUpdater() {
        if (databaseVersion == 0)
            return getDatabaseUpdater(NO_UPDATER);
        else
            return getDatabaseUpdater(databaseVersion - 1);
    }

    private DatabaseUpdater getDatabaseUpdater(int updaterVersion) {
        if (updaterVersion == DATABASE_LAST_VERSION)
            return updaters.get(updaters.size() - 1);

        if (updaterVersion == NO_UPDATER)
            return null;

        return updaters.get(updaterVersion);
    }
}
