package zero.utils.test;

import org.junit.rules.TestName;

import zero.environment.AbstractEnvironment;
import zero.environment.Environment;

public class CustomEnvironment extends ParametrizedTestRule<EnvironmentSetup> {

    private AbstractEnvironment originalEnvironment;

    public CustomEnvironment(TestName testName, Object testClass) {
        super(testName, testClass, EnvironmentSetup.class);
    }

    @Override
    protected void doBefore(EnvironmentSetup sutup) {
        if (sutup == null)
            return;

        String[] properties = sutup.value();

        int propertiesLength = properties.length;

        if ((propertiesLength % 2) != 0)
            throw new RuntimeException("EnvironmentSetup: value must have a pair number of arguments.");

        for (int i = 0; i < propertiesLength; i += 2) {
            String property = properties[i];
            String value = properties[i + 1];

            value = value.replace("%%HOME%%", Environment.get().getProperty("HOME"));

            Environment.get().setProperty(property, value);
        }
    }

    @Override
    protected void after() {
        Environment.set(originalEnvironment);

        super.after();
    }
}
