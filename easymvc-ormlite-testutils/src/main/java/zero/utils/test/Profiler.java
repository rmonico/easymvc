package zero.utils.test;

public class Profiler {

    private static long startTime;

    public static void check() {
        long endTime = System.currentTimeMillis();

        if (startTime > 0) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();

            StackTraceElement stackTraceElement = stackTrace[2];
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            String lineNumber = Integer.toString(stackTraceElement.getLineNumber());

            String place = className + "." + methodName + " (" + lineNumber + "): ";

            System.out.println(place + (endTime - startTime));
        } else {
            System.out.println("Profiler initialized");
        }
        startTime = System.currentTimeMillis();
    }

}
