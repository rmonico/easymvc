package zero.utils.test;

import org.junit.rules.TestName;

public class DBUnitDatasetFileNamesRule extends ParametrizedTestRule<DBUnitDatasetFileNames> {

    private String[] dbUnitDatasetFileNames;
    private String[] defaultDBUnitDatasetFileNames;

    public DBUnitDatasetFileNamesRule(TestName testName, Object testClass, String... defaultDBUnitDatasetFileNames) {
        super(testName, testClass, DBUnitDatasetFileNames.class);
        this.defaultDBUnitDatasetFileNames = defaultDBUnitDatasetFileNames;
    }

    @Override
    protected void doBefore(DBUnitDatasetFileNames dbUnitDatasetFileNamesAnnotation) {
        if (dbUnitDatasetFileNamesAnnotation == null)
            dbUnitDatasetFileNames = defaultDBUnitDatasetFileNames;
        else
            dbUnitDatasetFileNames = dbUnitDatasetFileNamesAnnotation.value();
    }

    public String[] getDBUnitDatasetFileNames() {
        return dbUnitDatasetFileNames;
    }

}
