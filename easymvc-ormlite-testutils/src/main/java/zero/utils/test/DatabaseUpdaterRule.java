package zero.utils.test;

import org.junit.rules.TestName;

class DatabaseUpdaterRule extends ParametrizedTestRule<UpdateDatabaseToVersion> {

    public int version;
    public boolean used;

    public DatabaseUpdaterRule(TestName testName, Object testClass) {
        super(testName, testClass, UpdateDatabaseToVersion.class);
    }

    @Override
    protected void doBefore(UpdateDatabaseToVersion testParameter) {
        used = testParameter != null;

        if (used)
            version = testParameter.value();
    }

}