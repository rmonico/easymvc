package zero.utils.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.RuleChain;
import org.junit.rules.TestName;

import zero.easymvc.EasyMVC;
import zero.easymvc.ormlite.ConnectionManager;
import zero.easymvc.ormlite.DaoManager;
import zero.easymvc.ormlite.DatabaseUpdater;
import zero.easymvc.ormlite.factory.AbstractApplicationFactory;
import zero.environment.ApplicationPropertyKeys;
import zero.environment.Environment;

public abstract class AbstractEasyMVCOrmliteTest {

    protected AbstractApplicationFactory factory;
    protected TestApplicationFactory testFactory;
    protected DaoManager daoManager;
    protected ConnectionManager connectionManager;
    protected EasyMVC controller;

    @Rule
    public RuleChain chain;

    public TestName testName;

    public CustomEnvironment environment;

    public ApplicationHomeFolderManager testAppHomeFolderManager;

    public DBUnitDatasetFileNamesRule dbUnitDatasetFileNames;

    public DatabaseUpdaterRule updaterRule;

    public AbstractEasyMVCOrmliteTest(String defaultHomeFolder, String... defaultDBUnitDatasetFileNames) {
        testName = new TestName();
        environment = new CustomEnvironment(testName, this);
        testAppHomeFolderManager = new ApplicationHomeFolderManager(testName, this, defaultHomeFolder);
        dbUnitDatasetFileNames = new DBUnitDatasetFileNamesRule(testName, this, defaultDBUnitDatasetFileNames);
        updaterRule = new DatabaseUpdaterRule(testName, this);

        chain = RuleChain.outerRule(testName).around(environment).around(testAppHomeFolderManager).around(dbUnitDatasetFileNames).around(updaterRule);
    }

    @Before
    public void initializeApplication() throws Exception {
        if (updaterRule.used)
            factory = createApplicationFactory(updaterRule.version);
        else
            factory = createApplicationFactory();

        factory.makeProperties();

        factory.makeLogger();

        connectionManager = factory.makeConnectionManager();

        daoManager = factory.makeDaoManager();

        controller = factory.makeController();

        doDatabaseApplicationUpdate();

        doDBUnitDatabaseUpdates();
    }

    private void doDatabaseApplicationUpdate() throws SQLException {
        DatabaseUpdater updater;

        if (updaterRule.used)
            updater = ((TestApplicationFactory) factory).getBeforeTestsDatabaseUpdater();
        else {
            updater = factory.getDatabaseUpdater();
        }

        if (updater != null)
            updater.update(-1);
    }

    private void doDBUnitDatabaseUpdates() throws ClassNotFoundException, SQLException, DatabaseUnitException, IOException {
        if (dbUnitDatasetFileNames.getDBUnitDatasetFileNames() != null) {
            String connectionString = Environment.get().getProperty(ApplicationPropertyKeys.JDBC_URL_KEY);
            String driverClassName = Environment.get().getProperty(ApplicationPropertyKeys.JDBC_DRIVER_CLASS_KEY);

            DBUnitTest dbUnitTest = new DBUnitTest(connectionString, driverClassName);

            dbUnitTest.initializeDBUnit(dbUnitDatasetFileNames.getDBUnitDatasetFileNames());
        }
    }

    protected abstract AbstractApplicationFactory createApplicationFactory(int databaseVersion);

    protected abstract AbstractApplicationFactory createApplicationFactory();

    protected IDataSet getDBUnitDataset() throws SQLException, DatabaseUnitException {
        String connectionString = connectionManager.getConnectionString();
        Connection jdbcConnection = DriverManager.getConnection(connectionString);

        DatabaseConnection connection = new DatabaseConnection(jdbcConnection);

        IDataSet databaseDataSet = connection.createDataSet();

        return databaseDataSet;
    }

}
