package zero.utils.test;

import java.io.File;

import org.junit.rules.TestName;

import zero.environment.Environment;

public class ApplicationHomeFolderManager extends ParametrizedTestRule<ApplicationHomeFolder> {

    private String defaultFolderName;
    private String homeFolderName;

    public ApplicationHomeFolderManager(TestName testName, Object testClass, String defaultFolderName) {
        super(testName, testClass, ApplicationHomeFolder.class);
        this.defaultFolderName = defaultFolderName;
    }

    @Override
    protected void doBefore(ApplicationHomeFolder homeFolderAnnotation) {
        if (homeFolderAnnotation == null)
            homeFolderName = defaultFolderName;
        else
            homeFolderName = homeFolderAnnotation.value();

        homeFolderName = homeFolderName.replace("%%HOME%%", Environment.get().getProperty("HOME"));
    }

    @Override
    protected void after() {
        removeWithChildren(new File(homeFolderName));

        super.after();
    }

    private void removeWithChildren(File file) {
        if (!file.exists())
            return;

        if (file.isFile())
            file.delete();
        else if (file.isDirectory()) {
            for (String subFileName : file.list())
                removeWithChildren(new File(file, subFileName));

            file.delete();
        }
    }

}
