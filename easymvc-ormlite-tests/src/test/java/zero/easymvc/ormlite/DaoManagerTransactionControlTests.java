package zero.easymvc.ormlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.SQLException;

import org.junit.Test;

import zero.easymvc.ArgumentsBean;
import zero.easymvc.CommandHandler;
import zero.easymvc.Dependency;
import zero.easymvc.EasyMVCException;
import zero.easymvc.Renderer;
import zero.easymvc.TokenParameter;
import zero.easymvc.ormlite.dao.MetaInfDao;
import zero.easymvc.ormlite.model.MetaInf;

public class DaoManagerTransactionControlTests extends EasyMVCOrmliteTest {

    public static class Arguments {
        @TokenParameter(token = "--fail")
        private boolean fail;
    }

    public static class Command {
        @Dependency
        private MetaInfDao dao;

        @ArgumentsBean
        private Arguments args;

        @CommandHandler(path = "command")
        public void run() throws SQLException {
            MetaInf key = new MetaInf();

            key.setKey("key");
            key.setValue("value");

            dao.create(key);

            if (args.fail)
                throw new RuntimeException("Last changed should be rolled back");
        }

        @Renderer(path = "command")
        public void render() {
        }
    }

    public static class TwoDependenciesOfSameManagerCommand {
        @Dependency
        private MetaInfDao dao;

        @Dependency
        private MetaInfDao anotherDao;

        @CommandHandler(path = "command")
        public void run() throws SQLException {
            MetaInf key = new MetaInf();

            key.setKey("key1");
            key.setValue("value of dao");

            dao.create(key);

            MetaInf anotherKey = new MetaInf();

            anotherKey.setKey("key2");
            anotherKey.setValue("value of another dao");

            anotherDao.create(anotherKey);
        }

        @Renderer(path = "command")
        public void render() {
        }
    }

    @Test
    public void should_rollback_transaction_on_exception() throws SQLException {
        controller.registerCommandHandler(Command.class);

        controller.registerRenderer(Command.class);

        RuntimeException threwException = null;
        try {
            controller.run("command", "--fail");
        } catch (EasyMVCException e) {
            Throwable cause = e.getCause();

            assertEquals(RuntimeException.class, cause.getClass());

            threwException = (RuntimeException) cause;
        }

        assertNotNull("A RuntimeException should be threw", threwException);
        assertEquals("Last changed should be rolled back", threwException.getMessage());

        MetaInfDao dao = MetaInfDao.getInstance(connectionManager.getConnection());

        assertNull(dao.getValue("key"));
    }

    @Test
    public void should_commit_transaction_after_command_returns() throws EasyMVCException, SQLException {
        controller.registerCommandHandler(TwoDependenciesOfSameManagerCommand.class);

        controller.registerRenderer(TwoDependenciesOfSameManagerCommand.class);

        controller.run("command");

        MetaInfDao dao = MetaInfDao.getInstance(connectionManager.getConnection());

        MetaInf value = dao.getValue("key1");

        assertNotNull(value);
        assertEquals("value of dao", value.getValue());

        MetaInf anotherValue = dao.getValue("key2");

        assertNotNull(anotherValue);
        assertEquals("value of another dao", anotherValue.getValue());
    }
}
