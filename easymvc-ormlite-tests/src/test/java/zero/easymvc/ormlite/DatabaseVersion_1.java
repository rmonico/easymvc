package zero.easymvc.ormlite;

import java.sql.SQLException;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseVersion_1 extends MetaInfUpdater {

    public DatabaseVersion_1(ConnectionSource connection) {
        super(connection);

        setUpdaterVersion(1);
    }

    @Override
    public void updateStructure() throws SQLException {
        super.updateStructure();

        TableUtils.createTableIfNotExists(connection, Version_1_Table.class);
    }

}
