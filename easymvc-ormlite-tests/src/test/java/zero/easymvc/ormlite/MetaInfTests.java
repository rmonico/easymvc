package zero.easymvc.ormlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import zero.easymvc.ormlite.dao.MetaInfDao;
import zero.easymvc.ormlite.model.MetaInf;

import com.j256.ormlite.support.ConnectionSource;

public class MetaInfTests extends EasyMVCOrmliteTest {

    public MetaInfTests() {
        super(new String[] { "dbunit/metainf_dataset.xml" });
    }

    @Test
    public void should_get_a_value_from_meta_inf_table() throws Exception {
        ConnectionSource connection = connectionManager.getConnection();

        MetaInfDao dao = MetaInfDao.getInstance(connection);

        MetaInf value = dao.getValue("some random key");

        assertNotNull(value);
        assertEquals("some random value", value.getValue());
    }
}
