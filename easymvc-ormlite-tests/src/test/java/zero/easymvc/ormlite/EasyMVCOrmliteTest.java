package zero.easymvc.ormlite;

import zero.easymvc.ormlite.factory.AbstractApplicationFactory;
import zero.utils.test.AbstractEasyMVCOrmliteTest;

public class EasyMVCOrmliteTest extends AbstractEasyMVCOrmliteTest {

    public EasyMVCOrmliteTest() {
        this(null);
    }

    public EasyMVCOrmliteTest(String[] defaultDBUnitDatasetFileNames) {
        super("%%HOME%%/.config/test_app", defaultDBUnitDatasetFileNames);
    }

    @Override
    protected AbstractApplicationFactory createApplicationFactory() {
        return new EasyMVCOrmliteTestApplicationFactory();
    }

    @Override
    protected AbstractApplicationFactory createApplicationFactory(int databaseVersion) {
        return new EasyMVCOrmliteTestApplicationFactory(databaseVersion);
    }

}
