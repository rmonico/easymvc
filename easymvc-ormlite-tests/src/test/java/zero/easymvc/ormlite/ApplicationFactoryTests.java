package zero.easymvc.ormlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;

import org.junit.Test;

import zero.environment.ApplicationPropertyKeys;
import zero.environment.Environment;
import zero.utils.test.ApplicationHomeFolder;
import zero.utils.test.EnvironmentSetup;

public class ApplicationFactoryTests extends EasyMVCOrmliteTest {

    private MyTestApplicationFactory factory;

    public class MyTestApplicationFactory extends EasyMVCOrmliteTestApplicationFactory {

        @Override
        public File getPropertiesFile() {
            return super.getPropertiesFile();
        }

    }

    @Override
    protected EasyMVCOrmliteTestApplicationFactory createApplicationFactory() {
        factory = new MyTestApplicationFactory();

        return factory;
    }

    @Test
    public void should_default_properties_be_set() throws Exception {
        assertEquals(ApplicationPropertyKeys.DEFAULT_LOGGER_VERBOSITY, Environment.get().getProperty(ApplicationPropertyKeys.LOGGER_VERBOSITY_PROPERTY_KEY));
    }

    @Test
    public void should_get_configuration_file_on_configs_folder_when_basename_is_set() throws Exception {
        File propertiesFile = factory.getPropertiesFile();

        assertNotNull(propertiesFile);

        String homeFolder = Environment.get().getProperty("HOME");

        assertEquals(homeFolder + "/.config/test_app/config.properties", propertiesFile.getPath());
    }

    @Test
    @EnvironmentSetup({ "TEST_APP_HOME", "%%HOME%%/test_app_alternate_home_folder" })
    @ApplicationHomeFolder("%%HOME%%/test_app_alternate_home_folder")
    public void should_get_configuration_file_based_on_environment_variable_when_its_set() throws Exception {
        File propertiesFile = factory.getPropertiesFile();

        assertNotNull(propertiesFile);

        String homeFolder = Environment.get().getProperty("HOME");

        assertEquals(homeFolder + "/test_app_alternate_home_folder/config.properties", propertiesFile.getPath());
    }

    @Test
    public void should_use_default_connection_settings_when_nothing_is_set() throws Exception {
        assertEquals("org.sqlite.JDBC", Environment.get().getProperty(ApplicationPropertyKeys.JDBC_DRIVER_CLASS_KEY));
        String defaultConnectionString = String.format("jdbc:sqlite:%s/database.sqlite", Environment.get().getProperty(ApplicationPropertyKeys.APP_HOME_PROPERTY_KEY));
        assertEquals(defaultConnectionString, connectionManager.getConnectionString());
    }

    @Test
    @EnvironmentSetup({ "TEST_APP_HOME", "%%HOME%%/test_app_alternate_home_folder" })
    @ApplicationHomeFolder("%%HOME%%/test_app_alternate_home_folder")
    public void should_use_changed_connection_settings_when_app_home_is_set() throws Exception {
        String homeFolder = Environment.get().getProperty("HOME");
        String defaultConnectionString = String.format("jdbc:sqlite:%s/database.sqlite", homeFolder + "/test_app_alternate_home_folder");
        assertEquals(defaultConnectionString, connectionManager.getConnectionString());
    }
}
