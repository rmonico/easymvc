package zero.easymvc.ormlite;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Version_1_Table {

    public static final String TABLE_NAME = "version_1_table";
    
    @DatabaseField(generatedId = true)
    private int id;

}
